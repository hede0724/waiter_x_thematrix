﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DelayBar : MonoBehaviour {

    #region variables

    [SerializeField]
    private Image delayBarValue;

    #endregion

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        bool keyPressed = Input.GetKey(KeyCode.Space);

        if (keyPressed)
        {
            if (delayBarValue.fillAmount > 0.0f)
                delayBarValue.fillAmount -= 0.01f;
        }
        else
            delayBarValue.fillAmount += 0.002f;
	}
}
