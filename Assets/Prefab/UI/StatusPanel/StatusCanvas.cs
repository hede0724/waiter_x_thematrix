﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatusCanvas : MonoBehaviour {

    #region UI_variables
    [SerializeField]
    private Text waveText;
    private string[] waveNumber;
    private int waveID;

    [SerializeField]
    private Text countDownText;
    [SerializeField]
    private Image countDownValue;

    [SerializeField]
    private Text statusText;
    #endregion

    #region variables

    /// <summary>
    /// CountDown Ongoing.
    /// </summary>
    bool onGoing;

    /// <summary>
    /// Game OnGoing / CD
    /// </summary>
    bool gameStatus;

    /// <summary>
    /// true : alpha up / false ; alpha down
    /// </summary>
    bool brighten;

    #endregion

    // Use this for initialization
    void Start () {
        waveNumber = new string[5];

        waveNumber[0] = "I";
        waveNumber[1] = "II";
        waveNumber[2] = "III";
        waveNumber[3] = "IV";
        waveNumber[4] = "V";

        onGoing = false;
        gameStatus = true;
        waveID = 0;

        brighten = false;
	}
	
	// Update is called once per frame
	void Update () {
        #region countDown
        if(!onGoing && waveID < 5)
        {
            waveText.text = waveNumber[waveID];
                   
            //  game onGoing
            if(gameStatus)
            {
                countDownValue.color = new Color(32.0f / 255.0f, 190.0f / 255.0f, 231.0f / 255.0f);
                countDownText.color = Color.white;
                statusText.text = "OnGoing";
                StartCoroutine( timerCountDown(30.0f) );

                waveID += 1;
            }
            //  CD
            else
            {
                countDownValue.color = new Color(233.0f / 255.0f, 122.0f / 255.0f, 22.0f / 255.0f);
                countDownText.color = Color.red;
                statusText.text = "Completed";
                StartCoroutine(timerCountDown(10.0f));
            }

            gameStatus = !gameStatus;
            onGoing = true;
        }
        #endregion

        #region blink
        if(brighten)
        {
            statusText.color = new Color(statusText.color.r,
                                         statusText.color.g,
                                         statusText.color.b,
                                         statusText.color.a + 0.05f
                                        );
            if (statusText.color.a > 1.0f)
                brighten = false;
        }   else
        {
            statusText.color = new Color(statusText.color.r,
                                         statusText.color.g,
                                         statusText.color.b,
                                         statusText.color.a - 0.05f
                                        );
            if (statusText.color.a < 0.3f)
                brighten = true;
        }
        #endregion
    }

    /// <summary>
    /// CountDown.
    /// </summary>
    /// <returns></returns>
    private IEnumerator timerCountDown(float countdownSeconds)
    {
        float previous = -1.0f;
        for(float i=0.0f; i< countdownSeconds; i+= Time.deltaTime * 2)
        {
            yield return new WaitForSeconds(Time.deltaTime);

            countDownValue.fillAmount = (countdownSeconds - i) / countdownSeconds;

            float floor = Mathf.Floor(i);

            if(previous != floor)
            {
                int timeValue = (int)countdownSeconds - (int)previous - 1;
                countDownText.text = "00 : " + timeValue.ToString("00");

                previous = floor;
            }
        }
        countDownText.text = "00 : 00";

        onGoing = false;
    }
}
